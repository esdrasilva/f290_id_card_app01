import 'package:flutter/material.dart';

//Importacao de dependencias externas url_launcher e font_awesome
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomePage(),
    );
  }
}

// Criamos Widget Statefull que representa a tela
class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  // Funcao utilizando dependencia externa URL_LAUNCHER
  Future _launchUrl() async {
    const url = 'https://flutter.dev';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Falha ao abrir url: $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // Alteracao da cor do plano de fundo do Scaffold
      backgroundColor: Colors.pinkAccent.shade400,

      // Criacao do Widgte principal do Layout, a Coluna
      body: Column(
        // Ajuste de alinhamento central para os elementos da Column
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(width: double.infinity),

          // Nome muito sugestivo...
          CircleAvatar(
            // Alteracao de cor de fundo com ajustes de tons
            backgroundColor: Colors.pinkAccent.shade100,
            radius: 95,
            child: CircleAvatar(
              // Obter imagem de Internet, poderiamos utilizar uma imagem local...
              backgroundImage: NetworkImage(
                  'https://images.pexels.com/photos/5211507/pexels-photo-5211507.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'),
              // Ajuste de raio menor para efeito de contorno no Avatar
              radius: 90,
            ),
          ),
          Text(
            'Yasmin Roussef',
            // COnfiguracao de estilo de fonte
            style: TextStyle(
              fontSize: 48,
              fontWeight: FontWeight.bold,
              color: Colors.white,
              // Selecao de familia de fonte customizada
              fontFamily: 'Yellowtail',
            ),
          ),
          Text(
            'FLUTTER DEVELOPER',
            style: TextStyle(
              fontSize: 24,
              fontWeight: FontWeight.bold,
              color: Colors.pinkAccent.shade100,
              fontFamily: 'PermanentMarker',
            ),
          ),
          // Widget Paddinga para reduzir o comprimento do Divider
          Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 75,
              vertical: 20,
            ),
            child: Divider(
              color: Colors.white,
              height: 5,
            ),
          ),

          // Criacao de Widget Card
          Card(
            margin: EdgeInsets.symmetric(horizontal: 50, vertical: 20),
            // Uso do ListTile para poder utilizar as propriedades Material Design
            child: ListTile(
              leading:
                  Icon(FontAwesomeIcons.linkedin, color: Colors.pinkAccent),
              title: Text(
                'linkedin/yasminroussef',
                style: TextStyle(fontSize: 18),
              ),
              onTap: () {
                // Chamada da funcao do URL_LAUNCHER para abrir o navegador nas plataformas iOS e Android com o mesmo codigo...
                _launchUrl();
              },
            ),
          ),
        ],
      ),
    );
  }
}

// Reutilizacao do botão Raised Button - Descartado... Uso apenas como exemplo.
class ButaoCustomizado extends StatelessWidget {
  final String textoBotao;
  const ButaoCustomizado(this.textoBotao);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      onPressed: () {},
      child: Text(textoBotao),
    );
  }
}
